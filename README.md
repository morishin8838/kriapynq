# KV260 PYNQセットアップ方法

## KV260 PYNQのセットアップ手順です。
* 以下の手順で、xilinx for ubuntu に kiria PYNQをインストールすることができます。
* ただし、2022.2.25現在、課題があり。upgradeは実行できません。コンフリクト解消できずです。

1. Xilinx for Ubuntu OS作成 SDカード作成
	1. balenaEtcherをダウンロードしいて起動
    2. Ubuntu for XilinxをダウンロードしてbalenaEtcherで書き込む
    	* https://ubuntu.com/download/xilinx
        * iot-kria-classic-desktop-2004-x03-20211110-98.img.xzを書き込む
        
2. KV260起動
    1. SDカード差し込み
    2. 外部LAN接続
    3. 電源ON
    4. ログイン 初期ユーザ名＆パスワード
		* username: ubuntu
        * password: ubuntu
        	- パスワードは必ず聞かれるので変更する。8文字以上で英数字混在      
	5. install basic 
		* $sudo apt install aptitude
		
	6. PYNQ DPUをインストール1回目 runtime 以外
		* $git clone https://github.com/Xilinx/Kria-PYNQ.git
		* $cd Kria-PYNQ/
		* $nano install.sh     
        	- #yes Y | apt remove --purge vitis-ai-runtime コメントアウトする
        	- #yes Y | apt remove vitis-ai-runtime #変更する
        * $sudo bash install.sh 
        	- ★ModuleNotFoundError: No module named 'vart'は無視して、次の作業に進む   
        
    7. KV260スタータキット
        1. Xilinx Development & Demonstration Environment for Ubuntu 20.04 LTS をインストール
        	* $sudo snap install xlnx-config --classic            
            * $sudo xlnx-config.sysinit            
        		- Xilinx environment setup is complete　インストール成功
				
	8. PYNQ DPUをインストール2回目 runtimeインストール
        * $nano install.sh     
        	- yes Y | apt install vitis-ai-runtime #変更して再度インストール
        	- yes Y | apt install vitis-ai-library #変更して再度インストール
        * $sudo bash install.sh     
			- Overwrite /root/.jupyter/jupyter_notebook_config.py with default config? [y/N]はyesです

	9. PYNQ 基本動作確認
        1. /etc/vart.confの作成 なぜか見つからないエラーが発生しますので、手動でファイルを作成します
            * $cd /etc
            * $sudo nano vart.conf
            	- firmware: /usr/lib/dpu.xclbin 
            	- を記載する。
            
        2. Jupyter起動
			* localhost:9090 or localhost:9090/lab
